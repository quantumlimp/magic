from dataclasses import dataclass

class MagicList():
	def __init__(self, data=None, cls_type=None):
		self.data = data if data else []
		self.cls_type = cls_type			

	def __setitem__(self, index, value):
		try:
			self.data[index] = value
		except IndexError as e:
			if not self.data and index == 0:
				self.data.append(value)
				return self.data[index]
			raise e

	def __getitem__(self, index):
		try:
			return self.data[index]
		except IndexError as e:
			if self.cls_type and index == 0:
				self.data.append(self.cls_type())					
		return self.data[index]

	def __str__(self):
		return str(self.data)

a = MagicList()
a[0] = 5
print(a)
#Uncomment to raise an error
#a[1] = 10

@dataclass
class Person:
	age: int = 1

b = MagicList(cls_type=Person)
b[0].age = 7
print(b)
print(b[0].age)
b[1].age = 10
print(b)

